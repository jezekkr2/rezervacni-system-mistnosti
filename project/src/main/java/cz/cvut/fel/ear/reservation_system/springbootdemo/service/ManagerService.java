package cz.cvut.fel.ear.reservation_system.springbootdemo.service;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.BuildingDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ManagerDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractUser;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Manager;
import cz.cvut.fel.ear.reservation_system.springbootdemo.security.SecurityUtils;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.security.AccessService;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Role;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ManagerService {

    private final ManagerDao managerDao;
    private final BuildingService buildingService;
    private final PasswordService passwordService;
    private final BuildingDao buildingDao;
    private final JavaMailSender javaMailSender;

    @Autowired
    public ManagerService(ManagerDao managerDao, BuildingService buildingService, PasswordService passwordService,
                          JavaMailSender javaMailSender, AccessService accessService, BuildingDao buildingDao) {

        this.managerDao = managerDao;
        this.buildingService = buildingService;
        this.passwordService = passwordService;
        this.javaMailSender = javaMailSender;
        this.buildingDao = buildingDao;
    }

    @Transactional
    public void create(Manager manager) {
        String password = passwordService.generatePassword();
        manager.setPassword(new BCryptPasswordEncoder().encode(password));
        managerDao.persist(manager);

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(manager.getEmail());
        msg.setSubject("Reservation system");
        msg.setText("Hello at conference room reservation system as new Manager\n Your password is: " + password + "\n Please change it as soon as possible." +
                "\n \n With love IT team.");
        javaMailSender.send(msg);
    }


    @Transactional
    public void update(Integer workerNumber, Manager manager) throws NotFoundException, NotAllowedException {

        Manager toUpdate = managerDao.findByManagerNumber(workerNumber);
        if (manager == null) throw new NotFoundException();
        if (manager.getRole() != null && manager.getRole() != Role.MANAGER) throw new NotAllowedException("You are not allowed to change role.");

        toUpdate.setFirstName(manager.getFirstName());
        toUpdate.setLastName(manager.getLastName());
        toUpdate.setUsername(manager.getUsername());
        toUpdate.setEmail(manager.getEmail());

        managerDao.update(toUpdate);
    }

    @Transactional(readOnly = true)
    public Manager find(Integer workerNumber) {

        return managerDao.findByManagerNumber(workerNumber);
    }

    @Transactional(readOnly = true)
    public Manager findMe() {
        final AbstractUser currentUser = SecurityUtils.getCurrentUser();
        return managerDao.find(currentUser.getId());
    }


    @Transactional(readOnly = true)
    public List<Manager> findAll() {
        return managerDao.findAll();
    }


    @Transactional(readOnly = true)
    public List<Manager> findAllByBlock(Long buildingName) throws NotFoundException {

        Building building = buildingService.find(buildingName);
        if (building == null) throw new NotFoundException();
        return building.getManagers();
    }


    @Transactional
    public void delete(Integer workerNumber) throws NotFoundException {

        Manager manager = managerDao.findByManagerNumber(workerNumber);
        if (manager == null) throw new NotFoundException();
        for (Building building : manager.getBuildings()) {
            building.removeManager(manager);
            buildingDao.update(building);
        }
        manager.softDelete();
        managerDao.update(manager);
    }

    @Transactional
    public int getNextManagerNumber(){
        List<Manager> managers = managerDao.findAll(true);
        if (managers == null || managers.isEmpty()) return 1;
        else {
            return managers.stream().reduce((manager, manager2) -> manager.getManagerNumber() > manager2.getManagerNumber() ? manager : manager2).get().getManagerNumber() + 1;
        }
    }

}
