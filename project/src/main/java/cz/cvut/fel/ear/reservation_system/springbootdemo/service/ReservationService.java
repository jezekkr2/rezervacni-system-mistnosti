package cz.cvut.fel.ear.reservation_system.springbootdemo.service;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ApplicantDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.BuildingDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ReservationDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.RoomDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.*;
import cz.cvut.fel.ear.reservation_system.springbootdemo.security.SecurityUtils;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.security.AccessService;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.security.RoleService;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
public class ReservationService {

    private ReservationDao reservationDao;
    private ApplicantDao applicantDao;
    private RoomDao roomDao;
    private RoomService roomService;
    private BuildingDao buildingDao;
    private AccessService accessService;
    private RoleService roleService;

    @Autowired
    public ReservationService(ReservationDao reservationDao, ApplicantDao applicantDao, RoomDao roomDao, RoomService roomService, BuildingDao buildingDao, AccessService accessService, RoleService roleService) {

        this.reservationDao = reservationDao;
        this.buildingDao = buildingDao;
        this.roomDao = roomDao;
        this.roomService = roomService;
        this.applicantDao = applicantDao;
        this.accessService = accessService;
        this.roleService = roleService;
    }

    public List<Reservation> findAll() { return reservationDao.findAll();  }

    public List<Reservation> findAll(Long buildingID) throws NotAllowedException, NotFoundException {
        accessService.managerAccess(buildingDao.find(buildingID));

        List<Reservation> reservationsInBuilding = new ArrayList<>();
        for (Reservation reservation: findAll()) {
            if (reservation.getRoom().getBuilding().getId().equals(buildingID)) reservationsInBuilding.add(reservation);
        }
        return reservationsInBuilding;
    }

    public Reservation findbyApplicant(Long applicantId) throws NotAllowedException {

        accessService.applicantAccess(applicantId);
        for (Reservation r: findAll()) {
            if (r.getApplicant().getId().equals(applicantId)) return r;
        }
        return null;
    }

    public Reservation find(Long id) {
        return reservationDao.find(id);
    }

    @Transactional
    public void cancelReservation(Reservation reservation) {
        setStatus(reservation, Status.CANCELLED);
    }

    @Transactional
    public void cancelReservation(Long id) throws NotFoundException {
        Reservation reservation = reservationDao.find(id);
        if (reservation == null) throw new NotFoundException();
        cancelReservation(reservation);
    }

    @Transactional
    public void createNewReservation(Reservation reservation, Long buildingId, int room_number, int numberOfPeople) throws NotFoundException, NotAllowedException {
        long id = SecurityUtils.getCurrentUser().getId();
        createNewReservation(reservation,id,buildingId,room_number, numberOfPeople);
    }

    @Transactional
    public void createNewReservation(Reservation reservation, Long applicantId, Long buildingId, int room_number, int numberOfPeople) throws NotFoundException, NotAllowedException {

        if (buildingId == null || reservation == null || applicantId==null) throw new NotFoundException();

        Applicant applicant = applicantDao.find(applicantId);
        Building building = buildingDao.find(buildingId);
        Room room = roomDao.findByRoomNumber(buildingId, room_number);
        if (applicant == null || building == null) throw new NotFoundException();
        accessService.managerAccess(building);
        accessService.applicantAccess(applicantId);

        if (room == null) throw new NotFoundException();
        if (roomService.isRoomFreeAtThisTime(room, reservation.getDateStart(), reservation.getDateEnd(), numberOfPeople)){
            reservation.setApplicant(applicant);
            reservation.setRoom(room);
        } else {
            throw new NotAllowedException("Reservation already exists.");
        }

    }

    @Transactional
    public void approveReservation(Long reservationId) throws NotFoundException, NotAllowedException {
        Reservation reservation = reservationDao.find(reservationId);
        if (reservation == null) throw new NotFoundException();
        accessService.managerAccess(reservation.getRoom().getBuilding());
        if (reservation.getStatus().equals(Status.PENDING)){
            reservation.setStatus(Status.ACTIVE);
            reservationDao.update(reservation);
        }
    }

    @Transactional
    public void createNewReservationRandom(Reservation reservation, long buildingId, int numberOfPeople) throws NotFoundException, NotAllowedException{
        long id = SecurityUtils.getCurrentUser().getId();
        createNewReservationRandom(reservation,id,buildingId,numberOfPeople);
    }

    @Transactional
    public void createNewReservationRandom(Reservation reservation, long applicantId, long buildingId, int numberOfPeople) throws NotFoundException, NotAllowedException {

        accessService.applicantAccess(applicantId);
        accessService.managerAccess(buildingDao.find(buildingId));

        List<Room> freeRooms = roomService.findFreeRooms(buildingId, reservation.getDateStart(), reservation.getDateEnd(), numberOfPeople);
        if (freeRooms != null){
            Room room = freeRooms.get(0);
            createNewReservation(reservation,applicantId,room.getBuilding().getId(),room.getRoomNumber(), numberOfPeople);
        }
    }

    @Scheduled(cron = "0 0 3 * * *", zone = "CET")
    @Transactional
    public void updateExpired() {

        for (Reservation reservation: findAll()) {
            if (reservation.getDateEnd().before(Timestamp.valueOf(LocalDateTime.now()))) {
                reservation.setStatus(Status.CANCELLED);
                reservationDao.update(reservation);
            }
        }
    }

    private void setStatus(Reservation reservation, Status status){
        reservation.setStatus(status);
        reservationDao.update(reservation);
    }

    @Transactional
    public void deleteReservation(Reservation reservation) throws NotFoundException, NotAllowedException {
        accessService.managerAccess(reservation.getRoom().getBuilding());
        if (reservation.getApplicant() != null)  reservation.getApplicant().cancelReservation(reservation);
        if (reservation.getRoom() != null) reservation.getRoom().cancelReservation(reservation);
        applicantDao.update(reservation.getApplicant());
        roomDao.update(reservation.getRoom());
        reservationDao.remove(reservation);
    }

    @Transactional
    public void deleteReservation(Long reservation_id) throws NotFoundException, NotAllowedException {
        Reservation reservation = reservationDao.find(reservation_id);
        if (reservation == null) throw new NotAllowedException();
        deleteReservation(reservation);
    }

    @Transactional
    public void deleteAllUserReservation() throws NotFoundException, NotAllowedException {
        AbstractUser user = SecurityUtils.getCurrentUser();
        if (roleService.isApplicant(user)){
            Applicant applicant = applicantDao.find(user.getId());
            deleteAllUserReservation(applicant.getId());
        }
    }

    @Transactional
    public void deleteAllUserReservation(Long applicantId) throws NotFoundException, NotAllowedException {
        Applicant applicant = applicantDao.find(applicantId);
        List<Reservation> reservations = applicant.getReservation();
        if (reservations != null) {
            for (int i = 0; i < reservations.size(); i++)
                deleteReservation(reservations.get(i));
        }
        else throw new NotFoundException();
        }
    }

