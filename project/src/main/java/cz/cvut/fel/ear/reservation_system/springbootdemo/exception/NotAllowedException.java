package cz.cvut.fel.ear.reservation_system.springbootdemo.exception;

public class NotAllowedException extends Exception{
    public NotAllowedException() {
        super("Forbidden operation.");
    }
    public NotAllowedException(String message) {
        super(message);
    }
}
