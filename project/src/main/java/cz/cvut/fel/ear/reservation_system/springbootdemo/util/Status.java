package cz.cvut.fel.ear.reservation_system.springbootdemo.util;

public enum Status {
    ACTIVE("ACTIVE"), PENDING("PENDING"), CANCELLED("CANCELLED"), ENDED("ENDED");

    private final String status;

    Status(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
