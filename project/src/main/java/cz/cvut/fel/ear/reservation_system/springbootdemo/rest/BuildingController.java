package cz.cvut.fel.ear.reservation_system.springbootdemo.rest;

import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.AlreadyExistsException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Room;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.BuildingService;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.RoomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/buildings")
@Validated
@PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_SUPERUSER')")
public class BuildingController {

    private static final Logger LOG = LoggerFactory.getLogger(BuildingController.class);

    private BuildingService buildingService;
    private RoomService roomService;


    @Autowired
    public BuildingController(BuildingService buildingService, RoomService roomService) {

        this.buildingService = buildingService;
        this.roomService = roomService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Building> getBuildings() {

        return buildingService.findAll();
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Building getBuildingByName(@PathVariable Long id) throws NotFoundException {

        Building building = buildingService.find(id);
        if (building == null) throw new NotFoundException();
        return building;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public void createBuilding(@RequestBody Building building) {

        buildingService.create(building);
        LOG.info("Building with id {} created.", building.getId());
    }


    @GetMapping(value = "/{buildingID}/floor/{number}/rooms", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Room> getRoomsWithSufficientCapacity(@PathVariable Long buildingID, @PathVariable Integer number) throws NotFoundException {

        return buildingService.getRoomsWithSufficientCapacity(buildingID,number);
    }

    @GetMapping(value = "/{buildingID}/rooms", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Room> getRoomsFromBuilding(@PathVariable Long buildingID) throws NotFoundException {
        return roomService.findAll(buildingID);
    }


    @PostMapping(value = "/{buildingID}/managers/{managerNumber}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public void addManagerToBuilding(@PathVariable Long buildingID, @PathVariable Long managerNumber) throws NotFoundException, AlreadyExistsException {

        buildingService.addManager(buildingID, managerNumber);
        LOG.info("Manager with manager number {} added to building {}.", managerNumber, buildingID);
    }


    @PatchMapping(value = "/{buildingID}/managers/{managerNumber}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public void removeManagerFromBuilding(@PathVariable Long buildingID, @PathVariable Long managerNumber) throws NotFoundException {

        buildingService.removeManager(buildingID, managerNumber);
        LOG.info("Manager with manager number {} removed from building {}", managerNumber, buildingID);
    }

    @PatchMapping(value = "/{buildingID}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public void updateBlock(@PathVariable Long buildingID, @RequestBody Map<String, String> request) throws NotFoundException {

        buildingService.update(buildingID, request.get("name"), request.get("address"));
        LOG.info("Building with ID {} updated.", buildingID);
    }

    @DeleteMapping(value = "/{buildingID}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public void removeBlock(@PathVariable Long buildingID, @RequestParam(defaultValue = "false") boolean accept) throws NotFoundException, NotAllowedException {
        if (accept){
            buildingService.delete(buildingID);
            LOG.info("Building with name {} removed.", buildingID);
        } else {
            LOG.info("Building {} not deleted. User not accept possible consequences of deleting.", buildingID);
        }
    }

}
