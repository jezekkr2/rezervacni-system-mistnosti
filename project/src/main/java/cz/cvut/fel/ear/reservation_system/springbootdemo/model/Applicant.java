package cz.cvut.fel.ear.reservation_system.springbootdemo.model;

import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Role;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Status;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Applicant extends AbstractUser{

    @Basic(optional = false)
    @Column(nullable = false)
    private LocalDate birth;

    @Basic(optional = true)
    @Column(nullable = true)
    private String bankAccountNumber;

    @OneToMany(mappedBy = "applicant", cascade = CascadeType.ALL)
    private List<Reservation> reservations;

    @Basic(optional = false)
    @Column(nullable = false)
    private String companyName;


    public Applicant() {
        setRole(Role.APPLICANT);
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public List<Reservation> getReservation() {
        
        if(reservations == null){
            return new ArrayList<>();
        }
        return reservations;
    }

    public void addReservation(Reservation reservation) {
        if(reservations == null){
            reservations = new ArrayList<>();
        }
        reservations.add(reservation);
    }

    public void cancelReservation(Reservation reservation){
        if (reservations.contains(reservation)){
            reservations.remove(reservation);
        }
    }

    public boolean hasActiveReservation(){
        if (reservations == null) return false;
        return reservations.stream().anyMatch(reservation -> reservation.getStatus().equals(Status.ACTIVE));
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
