package cz.cvut.fel.ear.reservation_system.springbootdemo.service.security;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.AbstractUserDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final AbstractUserDao userDao;

    @Autowired
    public UserDetailsService(AbstractUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final AbstractUser user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username " + username + " not found.");
        }
        return new cz.cvut.fel.ear.reservation_system.springbootdemo.security.model.UserDetails(user);
    }

}
