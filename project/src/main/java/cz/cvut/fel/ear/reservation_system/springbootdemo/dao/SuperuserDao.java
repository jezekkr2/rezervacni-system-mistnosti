package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Superuser;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Objects;

@Repository
public class SuperuserDao extends BaseDao<Superuser> {
    public SuperuserDao(){super(Superuser.class);
    }
}
