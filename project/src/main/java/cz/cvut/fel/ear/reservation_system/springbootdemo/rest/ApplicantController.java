package cz.cvut.fel.ear.reservation_system.springbootdemo.rest;

import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Applicant;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.ApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/applicants")
@Validated
@PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_SUPERUSER')")
public class ApplicantController {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicantController.class);
    private ApplicantService applicantService;

    @Autowired
    public ApplicantController(ApplicantService service) {

        this.applicantService = service;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Applicant> getApplicants() {

        return applicantService.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Applicant getApplicant(@PathVariable Long id) throws NotFoundException {

        Applicant applicant = applicantService.find(id);
        if (applicant == null) throw new NotFoundException();
        return applicant;
    }

    @PreAuthorize("hasRole('ROLE_APPLICANT')")
    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public Applicant getApplicantMe() throws NotFoundException {

        Applicant applicant = applicantService.findMe();
        if (applicant == null) throw new NotFoundException();
        return applicant;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createApplicant(@Valid @RequestBody Applicant applicant) throws NotAllowedException {

        applicantService.create(applicant);
        LOG.info("Applicant with id {} created.", applicant.getId());
    }

    @PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateApplicant(@PathVariable Long id, @RequestBody Applicant student) throws NotFoundException, NotAllowedException {

        applicantService.update(id, student);
        LOG.info("Applicant with id {} updated.", id);
    }

    @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public void removeApplicant(@PathVariable Long id) throws NotFoundException, NotAllowedException {

        applicantService.delete(id);
        LOG.info("Applicant with id {} removed.", id);
    }

}
