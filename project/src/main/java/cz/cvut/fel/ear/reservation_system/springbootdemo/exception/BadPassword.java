package cz.cvut.fel.ear.reservation_system.springbootdemo.exception;

public class BadPassword  extends Exception{
    public BadPassword() { super("Wrong password."); }
}