package cz.cvut.fel.ear.reservation_system.springbootdemo.model;

import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Status;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;

@Entity
public class Reservation extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    @JoinColumn(name="applicant_id", nullable=false)
    private Applicant applicant;

    @OneToOne
    @JoinColumn(name="room_id", nullable=false)
    private Room room;

    @Basic(optional = false)
    @Column(nullable = false)
    private Timestamp dateStart;

    @Basic(optional = false)
    @Column(nullable = false)
    private Timestamp dateEnd;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Status getStatus() {

        return status;
    }


    public void setStatus(Status status) {

        this.status = status;
    }
}
