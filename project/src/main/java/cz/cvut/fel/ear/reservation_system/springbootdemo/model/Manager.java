package cz.cvut.fel.ear.reservation_system.springbootdemo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.util.List;

@Entity
public class Manager extends AbstractUser{

    @Basic(optional = false)
    @Column(unique = true, nullable = false)
    private Integer managerNumber;

    @ManyToMany(mappedBy = "managers", cascade = CascadeType.ALL)
    private List<Building> buildings;

    public Integer getManagerNumber() { return managerNumber; }

    public void setManagerNumber(Integer workerNumber) { this.managerNumber = workerNumber; }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void addBuilding(Building building) { buildings.add(building); }

    public void removeBuilding(Building building) {buildings.remove(building);}
}
