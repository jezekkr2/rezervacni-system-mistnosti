package cz.cvut.fel.ear.reservation_system.springbootdemo.model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Superuser extends AbstractUser{

    @OneToMany
    private List<Manager> managers;

}
