package cz.cvut.fel.ear.reservation_system.springbootdemo.util;

public enum Role {
    APPLICANT("ROLE_APPLICANT"), MANAGER("ROLE_MANAGER"),SUPERUSER("ROLE_SUPERUSER");

    private final String role;

    Role(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}