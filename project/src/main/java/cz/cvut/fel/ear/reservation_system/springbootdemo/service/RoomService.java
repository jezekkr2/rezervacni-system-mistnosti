package cz.cvut.fel.ear.reservation_system.springbootdemo.service;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ApplicantDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.BuildingDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.RoomDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.AlreadyExistsException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Applicant;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Reservation;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Room;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Status;
import javassist.bytecode.analysis.ControlFlow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class RoomService {

    private static final Logger LOG = LoggerFactory.getLogger(RoomService.class);
    private final BuildingDao buildingDao;
    private final RoomDao roomDao;
    private ApplicantDao applicantDao;

    @Autowired
    public RoomService(BuildingDao buildingDao, RoomDao roomDao, ApplicantDao applicantDao) {

        this.buildingDao = buildingDao;
        this.roomDao = roomDao;
        this.applicantDao = applicantDao;
    }

    @Transactional
    public List<Room> findAll(Long buildingID) throws NotFoundException {
        Building building = buildingDao.find(buildingID);
        if (building == null) throw new NotFoundException();
        return building.getRooms();
    }

    @Transactional
    public List<Reservation> getReservations(Long buildingID, Integer roomNumber) throws NotFoundException {
        Room room = roomDao.findByRoomNumber(buildingID, roomNumber);
        return room.getReservations();
    }


    @Transactional
    public List<Room> findFreeRooms(Long buildingID, Timestamp dateStart, Timestamp dateEnd, int numberOfPeople) throws NotFoundException {

        List<Room> freeRoomList = new ArrayList<Room>();
        Building building = buildingDao.find(buildingID);
        if (building == null) throw new NotFoundException();
        if (building.getRooms() == null) throw new NotFoundException();
        List<Room> roomsInBuilding = building.getRooms();

        for (int i = 0; i < roomsInBuilding.size(); i++) {

            Room selectedRoom = roomsInBuilding.get(i);
            if(isRoomFreeAtThisTime(selectedRoom, dateStart, dateEnd, numberOfPeople)) freeRoomList.add(selectedRoom);

        }
        return freeRoomList;
    }

    public boolean isRoomFreeAtThisTime(Room room, Timestamp dateStart, Timestamp dateEnd, int numberOfPeople){

        ArrayList<Reservation> reservations = room.getReservations();
        Boolean isFree = true;

        if(numberOfPeople > room.getCapacity()) isFree = false;

        for(int i = 0; i < reservations.size(); i++){
                Timestamp reservationStart = reservations.get(i).getDateStart();
                Timestamp reservationEnd = reservations.get(i).getDateEnd();

                if(dateStart.before(reservationEnd) && reservationEnd.before(dateEnd)){
                    isFree = false;
                }

        }
        return isFree;
    }

    public Room find(Long buildingID, Integer roomNumber) throws NotFoundException {
        Building building = buildingDao.find(buildingID);
        Room room = roomDao.findByRoomNumber(buildingID, roomNumber);
        if (building == null || room == null) throw new NotFoundException();
        return room;
    }

    @Transactional
    public void addRoom(Long buildingID, Room room) throws NotFoundException, AlreadyExistsException, NotAllowedException {

        Building building = buildingDao.find(buildingID);
        if (building == null || room == null) throw new NotFoundException();

        List<Room> rooms = findAll(building.getId());

        room.setBuilding(building);

        boolean roomExist = rooms.stream().anyMatch(findingRoom -> findingRoom.getRoomNumber().equals(room.getRoomNumber()));


        if (!roomExist) {
            roomDao.persist(room);
            building.addRoom(room);
            buildingDao.update(building);
        } else {
            LOG.error("Room number " + room.getRoomNumber() + " at building " + building.getId() + " already exists.");
            throw new AlreadyExistsException();
        }
    }

    @Transactional
    public void removeAllReservations(Room room) {
        List<Reservation> res = room.getReservations();
        for (int i = 0; i < res.size(); i++) {
            Reservation r = res.get(i);
            room.cancelReservation(r);
        }
        roomDao.update(room);
    }

    @Transactional
    public Reservation getReservation(Room room, Applicant applicant) throws NotFoundException {

        if (room == null) throw new NotFoundException();
        if (room.getReservations() == null) return null;

        for (Reservation reservation : room.getReservations()) {
            if (reservation.getStatus() == Status.ACTIVE && reservation.getApplicant().equals(applicant)) {
                return reservation;
            }
        }
        return null;
    }

    @Transactional
    public Timestamp suggestOtherTime(Room room, Timestamp dateStart, Timestamp dateEnd, int numberOfPeople) throws NotFoundException {

        while(true){
            if(isRoomFreeAtThisTime(room, dateStart, dateEnd, numberOfPeople)){
                return dateStart;
            } else {
                long addTime = 1*60*60*1000;
                dateStart.setTime(dateStart.getTime() + TimeUnit.HOURS.toMillis(addTime));
                dateEnd.setTime(dateEnd.getTime() + TimeUnit.HOURS.toMillis(addTime));
            }
        }

    }

    @Transactional
    public void deleteRoom(Integer roomNumber, Long buildingID) throws NotFoundException, NotAllowedException {
        Building building = buildingDao.find(buildingID);
        Room room = roomDao.findByRoomNumber(buildingID, roomNumber);
        if (building == null || room == null) throw new NotFoundException();
        deleteRoom(room);
    }

    @Transactional
    public void deleteRoom(Room room) throws NotAllowedException, NotFoundException {

        removeAllReservations(room);

        Building building = room.getBuilding();
        building.removeRoom(room);
        buildingDao.update(building);

        room.setBuilding(null);
        room.softDelete();

        LOG.info("Room " + room.getRoomNumber() + " at building " + building.getName() + " was deleted.");

        roomDao.update(room);
    }


}
