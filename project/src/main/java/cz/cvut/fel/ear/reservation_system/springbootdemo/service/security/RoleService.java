package cz.cvut.fel.ear.reservation_system.springbootdemo.service.security;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractUser;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Role;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    public boolean isApplicant(AbstractUser user){
        return user.getRole() == Role.APPLICANT;
    }

    public boolean isManager(AbstractUser user){
        return user.getRole() == Role.MANAGER || user.getRole() == Role.SUPERUSER;
    }
}
