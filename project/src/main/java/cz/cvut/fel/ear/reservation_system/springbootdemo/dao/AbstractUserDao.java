package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractUser;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class AbstractUserDao extends BaseDao<AbstractUser> {
    public AbstractUserDao(){super(AbstractUser.class);}

    public AbstractUser findByUsername(String username){
        {
            try {
                return em.createNamedQuery("AbstractUser.findByUsername", AbstractUser.class).setParameter("username", username)
                        .getSingleResult();
            } catch (NoResultException e) {
                return null;
            }
        }
    }
}
