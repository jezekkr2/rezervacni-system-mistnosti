package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Manager;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Objects;

@Repository
public class RoomDao extends BaseDao<Room> {
    public RoomDao(){super(Room.class);}

    public Room findByRoomNumber(Long buildingID, Integer roomNumber) {
        try {
            return em.createNamedQuery("Room.find", Room.class).setParameter("buildingID", buildingID)
                    .setParameter("roomNumber", roomNumber)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
