package cz.cvut.fel.ear.reservation_system.springbootdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemestralkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SemestralkaApplication.class, args);
    }

}
