package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Reservation;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class ReservationDao extends BaseDao<Reservation> {
    public ReservationDao(){super(Reservation.class);}

}
