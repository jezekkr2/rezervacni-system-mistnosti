package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Applicant;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Objects;

@Repository
public class ApplicantDao extends BaseDao<Applicant> {
    public ApplicantDao() {
        super(Applicant.class);
    }
}

