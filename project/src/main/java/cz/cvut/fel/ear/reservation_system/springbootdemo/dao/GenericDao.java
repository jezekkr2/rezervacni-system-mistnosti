package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface GenericDao<T extends AbstractEntity> {

    T find(Long id);

    List<T> findAll();

    void persist(T entity);

    void persist(Collection<T> entities);

    T update(T entity);

    void remove(T entity);

    boolean exists(Long id);
}
