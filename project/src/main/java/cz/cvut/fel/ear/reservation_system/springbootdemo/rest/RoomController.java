package cz.cvut.fel.ear.reservation_system.springbootdemo.rest;

import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.AlreadyExistsException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Reservation;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Room;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.RoomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/rooms")
@Validated
@PreAuthorize("hasAnyRole('ROLE_MANAGER','ROLE_SUPERUSER')")
public class RoomController {

    private static final Logger LOG = LoggerFactory.getLogger(RoomController.class);
    private RoomService roomService;

    @Autowired
    public RoomController(RoomService service) {
        this.roomService = service;
    }

    @GetMapping(value = "/building/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Room> getRooms(@PathVariable Long id) throws NotFoundException {

        return roomService.findAll(id);
    }

    @GetMapping(value = "/{number}/building/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Room getRoom(@PathVariable Integer number, @PathVariable Long id) throws NotFoundException {

        return roomService.find(id, number);
    }

    @GetMapping(value = "/{number}/building/{id}/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservationsInRoom(@PathVariable Integer number, @PathVariable Long id) throws NotFoundException {

        return roomService.getReservations(id,number);
    }

    @GetMapping(value = "/free/building/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Room> getFreeRooms(@PathVariable Long id, @RequestParam Timestamp dateStart, @RequestParam Timestamp dateEnd, @PathVariable Integer numOfPeople) throws NotFoundException
    {
        return roomService.findFreeRooms(id, dateStart, dateEnd, numOfPeople);
    }

    @PostMapping(value = "/building/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createRoom(@RequestBody Room room, @PathVariable Long id) throws NotFoundException, AlreadyExistsException, NotAllowedException {

        roomService.addRoom(id,room);
        LOG.info("Room number {} created at building {}.", room.getRoomNumber(),room.getBuilding().getName());
    }

    @DeleteMapping(value = "/{number}/building/{buildingID}",consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRoom(@PathVariable Integer number, @PathVariable Long buildingID, @RequestParam(defaultValue = "false") boolean accept) throws NotFoundException, AlreadyExistsException, NotAllowedException {
        if (accept){
            roomService.deleteRoom(number,buildingID);
            LOG.info("Room number {} at building {} was deleted.", number, buildingID);
        } else {
            LOG.info("Room number {} at building {} was deleted. User not accept possible consequences of deleting.", number, buildingID);
        }
    }

}
