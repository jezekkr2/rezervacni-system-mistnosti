package cz.cvut.fel.ear.reservation_system.springbootdemo.service;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ApplicantDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractUser;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Applicant;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Reservation;
import cz.cvut.fel.ear.reservation_system.springbootdemo.security.SecurityUtils;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.security.AccessService;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Role;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ApplicantService {

    private final ApplicantDao applicantDao;
    private final PasswordService passwordService;
    private final AccessService accessService;
    private final JavaMailSender javaMailSender;

    @Autowired
    public ApplicantService(ApplicantDao applicantDao, PasswordService passwordService, AccessService accessService, JavaMailSender javaMailSender) {
        this.applicantDao = applicantDao;
        this.passwordService = passwordService;
        this.accessService = accessService;
        this.javaMailSender = javaMailSender;
    }

    public List<Applicant> findAll() {
        return applicantDao.findAll();
    }

    public Applicant find(Long id) {
        return applicantDao.find(id);
    }

    public Applicant findMe() {
        final AbstractUser currentUser = SecurityUtils.getCurrentUser();
        return applicantDao.find(currentUser.getId());
    }

    @Transactional
    public void create(Applicant applicant) {
        String password = passwordService.generatePassword();
        applicant.setPassword(new BCryptPasswordEncoder().encode(password));
        applicantDao.persist(applicant);

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(applicant.getEmail());
        msg.setSubject("Reservation system");
        msg.setText("Hello at conference room reservation system \n Your password is: " + password + "\n Please change it as soon as possible." +
                "\n \n With love IT team.");
        javaMailSender.send(msg);
    }


    @Transactional
    public void update(Long id, Applicant applicant) throws NotFoundException, NotAllowedException {
        Applicant applicantToUpdate = applicantDao.find(id);
        if (applicantToUpdate == null) throw new NotFoundException();
        if (applicant.getRole() != Role.APPLICANT) throw new NotAllowedException("You are not allowed to change role.");

        applicantToUpdate.setFirstName(applicant.getFirstName());
        applicantToUpdate.setLastName(applicant.getLastName());
        applicantToUpdate.setUsername(applicant.getUsername());
        applicantToUpdate.setEmail(applicant.getEmail());
        applicantToUpdate.setBankAccountNumber(applicant.getBankAccountNumber());
        applicantToUpdate.setBirth(applicant.getBirth());
        applicantDao.update(applicantToUpdate);
    }


    @Transactional
    public void delete(Long id) throws NotFoundException, NotAllowedException {

        Applicant applicant = applicantDao.find(id);
        if (applicant == null) throw new NotFoundException();
        if (applicant.hasActiveReservation()) {
            throw new NotAllowedException("You cannot delete applicant with active reservation.");
        } else {
            if (applicant.getReservation() != null){
                List<Reservation> reservations = applicant.getReservation();
                for(int i = 0; i < reservations.size(); i++){
                    reservations.get(i).setStatus(Status.CANCELLED);
                }
            }
            applicant.softDelete();
            applicantDao.update(applicant);
        }
    }

}
