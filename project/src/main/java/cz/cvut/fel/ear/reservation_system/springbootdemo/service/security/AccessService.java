package cz.cvut.fel.ear.reservation_system.springbootdemo.service.security;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.AbstractUserDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ManagerDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.BadPassword;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.AbstractUser;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Manager;
import cz.cvut.fel.ear.reservation_system.springbootdemo.security.SecurityUtils;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccessService {
    private final ManagerDao managerDao;
    private final PasswordEncoder passwordEncoder;
    private final JavaMailSender javaMailSender;
    private AbstractUserDao userDao;

    @Autowired
    public AccessService(ManagerDao managerDao, PasswordEncoder passwordEncoder, JavaMailSender javaMailSender, AbstractUserDao userDao) {
        this.managerDao = managerDao;
        this.passwordEncoder = passwordEncoder;
        this.javaMailSender = javaMailSender;
        this.userDao = userDao;
    }

    @Transactional
    public void managerAccess(Building building) throws NotAllowedException, NotFoundException {
        final AbstractUser currentUser = SecurityUtils.getCurrentUser();
        if (building == null) throw new NotFoundException();
        if (currentUser.getRole().equals(Role.MANAGER)) {
            Manager manager = managerDao.find(currentUser.getId());
            if (!manager.getBuildings().contains(building)) { throw new NotAllowedException("Access denied."); }
        }
    }

    @Transactional
    public void applicantAccess(Long student_id) throws NotAllowedException {

        final AbstractUser currentUser = SecurityUtils.getCurrentUser();
        if (currentUser.getRole().equals(Role.APPLICANT)) {
            if (!currentUser.getId().equals(student_id)) throw new NotAllowedException("Access denied.");
        }
    }

    @Transactional
    public void changePassword(String oldPassword, String newPassword, String newPasswordAgain) throws BadPassword {

        final AbstractUser currentUser = SecurityUtils.getCurrentUser();
        if (!passwordEncoder.matches(oldPassword, userDao.find(currentUser.getId()).getPassword())) {
            throw new BadPassword();
        }else {
            if (newPassword.equals(newPasswordAgain)) {
                currentUser.setPassword(new BCryptPasswordEncoder().encode(newPassword));
                SimpleMailMessage msg = new SimpleMailMessage();
                msg.setTo(currentUser.getEmail());
                msg.setSubject("Password change");
                msg.setText("Hello your password has been changed.\n" + "If you did not change it, contact us as soon as possible." +
                        "\n \n With love IT team.");
                javaMailSender.send(msg);
                userDao.update(currentUser);
            }
        }
    }
}
