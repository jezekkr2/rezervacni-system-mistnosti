package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Manager;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Objects;

@Repository
public class ManagerDao extends BaseDao<Manager> {
    public ManagerDao() {
        super(Manager.class);
    }

    @Override
    public Manager find(Long id) {
        Objects.requireNonNull(id);
        Manager a = em.find(type, id);
        if (a != null && a.isNotDeleted()) return a;
        return null;
    }

    public Manager findByManagerNumber(Integer workerNumber) {
        try {
            return em.createNamedQuery("Manager.findByManagerNumber", Manager.class).setParameter("workerNumber", workerNumber)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
