package cz.cvut.fel.ear.reservation_system.springbootdemo.exception;

public class AlreadyExistsException extends Exception{

    public AlreadyExistsException() {
        super("Already exists.");
    }
}
