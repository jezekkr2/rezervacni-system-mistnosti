package cz.cvut.fel.ear.reservation_system.springbootdemo.dao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Objects;

@Repository
public class BuildingDao extends BaseDao<Building> {
    public BuildingDao() {
        super(Building.class);
    }

    @Override
    public Building find(Long id) {
        Objects.requireNonNull(id);
        Building a = em.find(type, id);
        if (a != null && a.isNotDeleted()) return a;
        return null;
    }
}
