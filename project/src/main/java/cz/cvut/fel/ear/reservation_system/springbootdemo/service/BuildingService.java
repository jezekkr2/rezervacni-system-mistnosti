package cz.cvut.fel.ear.reservation_system.springbootdemo.service;

import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.BuildingDao;
import cz.cvut.fel.ear.reservation_system.springbootdemo.dao.ManagerDao;

import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.AlreadyExistsException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
//import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.WrongCapacityException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Building;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Manager;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BuildingService {

    private final BuildingDao buildingDao;
    private final ManagerDao managerDao;
    private final RoomService roomService;

    @Autowired
    public BuildingService(BuildingDao buildingDao, ManagerDao managerDao, RoomService roomService) {

        this.buildingDao = buildingDao;
        this.managerDao = managerDao;
        this.roomService = roomService;

    }

    public List<Building> findAll() {
        return buildingDao.findAll();
    }


    public Building find(Long id) {
        return buildingDao.find(id);
    }

    @Transactional
    public void create(Building building) {
        buildingDao.persist(building);
    }


    @Transactional
    public void addManager(Long buildingID, Long managerID) throws NotFoundException, AlreadyExistsException {

        Building building = buildingDao.find(buildingID);
        Manager manager = managerDao.find(managerID);
        if (building == null || manager == null) throw new NotFoundException();

        building.addManager(manager);
        manager.addBuilding(building);

        buildingDao.update(building);
        managerDao.update(manager);

    }

    @Transactional
    public void removeManager(Long buildingID, Long managerID) throws NotFoundException {

        Building building = buildingDao.find(buildingID);
        Manager manager = managerDao.find(managerID);
        if (building == null || manager == null) throw new NotFoundException();

        building.removeManager(manager);
        manager.removeBuilding(building);

        buildingDao.update(building);
        managerDao.update(manager);

    }

    @Transactional
    public List<Room> getRoomsWithSufficientCapacity(Long buildingID, Integer capacity) throws NotFoundException {
        Building building = buildingDao.find(buildingID);
        if (building == null) throw new NotFoundException();
        return building.getRooms().stream().filter(room -> room.getCapacity() >= capacity).collect(Collectors.toList());
    }


    @Transactional
    public void update(Long buildingID, String name, String address) throws NotFoundException {
        Building building = buildingDao.find(buildingID);
        if (building == null) throw new NotFoundException();
        if (name != null) building.setName(name);
        if (address != null) building.setAddress(address);
    }

    @Transactional
    public void delete(Long buildingID) throws NotFoundException, NotAllowedException {

        Building building = buildingDao.find(buildingID);
        if (building == null) throw new NotFoundException();
        List<Room> toDeleteList;
        if (building.getRooms()!= null)  {
            toDeleteList = new ArrayList<>(building.getRooms());
            for (Room toDelete: toDeleteList) {
                roomService.deleteRoom(toDelete);
            }
        }
        building.softDelete();
        buildingDao.update(building);
    }

}
