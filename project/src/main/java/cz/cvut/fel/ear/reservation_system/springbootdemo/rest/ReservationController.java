package cz.cvut.fel.ear.reservation_system.springbootdemo.rest;

import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Reservation;
import cz.cvut.fel.ear.reservation_system.springbootdemo.security.SecurityUtils;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reservations")
@Validated
public class ReservationController {

    private ReservationService reservationService;
    private static final Logger LOG = LoggerFactory.getLogger(ReservationController.class);


    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPERUSER', 'ROLE_MANAGER')")
    @GetMapping(value = "/building/{buildingID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservations(@PathVariable Long buildingID) throws NotFoundException, NotAllowedException {

        return reservationService.findAll(buildingID);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPERUSER', 'ROLE_MANAGER', 'ROLE_APPLICANT')")
    @GetMapping(value = "/applicant/{applicant_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Reservation getReservation(@PathVariable Long applicant_id) throws NotAllowedException {

        return reservationService.findbyApplicant(applicant_id);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPERUSER', 'ROLE_MANAGER')")
    @PatchMapping(value = "{id}/approve", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void approveReservation(@PathVariable Long id) throws NotAllowedException, NotFoundException {

        reservationService.approveReservation(id);
        LOG.info("Reservation with id {} approved", id);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPERUSER', 'ROLE_MANAGER')")
    @PatchMapping(value = "{id}/denied", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void deniedReservation(@PathVariable Long id) throws NotFoundException {

        reservationService.cancelReservation(id);
        LOG.info("Reservation with id {} denied", id);
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPERUSER', 'ROLE_MANAGER')")
    @PostMapping(value = "/applicant/{applicant_id}/buildings/{buildingID}/rooms/{room_number}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createReservation(@RequestBody Reservation reservation, @PathVariable Long applicant_id, @PathVariable int buildingID, @PathVariable Integer room_number) throws NotFoundException, NotAllowedException{

        reservationService.createNewReservation(reservation, applicant_id ,buildingID, room_number);
        LOG.info("Reservation with id {} created", reservation.getId());
    }

    @PreAuthorize("hasRole('ROLE_APPLICANT')")
    @PostMapping(value = "/buildings/{buildingID}/rooms/{room_number}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createApplicantReservation(@RequestBody Reservation reservation, @PathVariable Long buildingID, @PathVariable Integer room_number, @PathVariable Integer numOfPeople) throws NotFoundException, NotAllowedException{

        reservationService.createNewReservation(reservation,buildingID, room_number, numOfPeople);
        LOG.info("Reservation with id {} created", reservation.getId());
    }

    @PreAuthorize("hasRole('ROLE_APPLICANT')")
    @PostMapping(value = "/buildings/{buildingID}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createReservationApplicantRandom(@RequestBody Reservation reservation,  @PathVariable Long buildingID, @PathVariable Integer numOfPeople) throws NotFoundException, NotAllowedException{

        reservationService.createNewReservationRandom(reservation, buildingID, numOfPeople);
        LOG.info("Reservation on room {} created", reservation.getRoom().getRoomNumber());
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPERUSER', 'ROLE_MANAGER')")
    @PostMapping(value = "/applicant/{applicant_id}/buildings/{buildingID}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createReservationRandom(@RequestBody Reservation reservation, @PathVariable Long applicant_id, @PathVariable Long buildingID, @PathVariable Integer numOfPeople) throws NotFoundException, NotAllowedException{

        reservationService.createNewReservationRandom(reservation, applicant_id, buildingID, numOfPeople);
        LOG.info("Reservation on room {} created", reservation.getRoom().getRoomNumber());
    }

    @PreAuthorize("hasRole('ROLE_APPLICANT')")
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelAllUserReservation(@RequestParam(defaultValue = "false") boolean accept) throws NotFoundException, NotAllowedException {
        if (accept){
            reservationService.deleteAllUserReservation();
            LOG.info("Reservation of Applicant with username " + SecurityUtils.getCurrentUser().getUsername() + " deleted");
        } else {
            LOG.info("Reservation was not deleted. User not accept possible consequences of deleting.");
        }
    }

}
