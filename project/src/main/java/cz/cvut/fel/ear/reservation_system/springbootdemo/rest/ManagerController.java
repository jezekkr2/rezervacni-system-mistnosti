package cz.cvut.fel.ear.reservation_system.springbootdemo.rest;

import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.AlreadyExistsException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotAllowedException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.exception.NotFoundException;
import cz.cvut.fel.ear.reservation_system.springbootdemo.model.Manager;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.BuildingService;
import cz.cvut.fel.ear.reservation_system.springbootdemo.service.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/managers")
@Validated
@PreAuthorize("hasRole('ROLE_SUPERUSER')")
public class ManagerController {

    private static final Logger LOG = LoggerFactory.getLogger(ManagerController.class);
    private final ManagerService managerService;
    private final BuildingService buildingService;


    @Autowired
    public ManagerController(ManagerService managerService, BuildingService buildingService) {

        this.managerService = managerService;
        this.buildingService = buildingService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Manager> getManagers() {

        return managerService.findAll();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public Manager getApplicantMe() throws NotFoundException {

        Manager manager = managerService.findMe();
        if (manager == null) throw new NotFoundException();
        return manager;
    }

    @GetMapping(value = "/{managerNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Manager getManager(@PathVariable Integer workerNumber) {

        return managerService.find(workerNumber);
    }



    @PostMapping(value = "/{managerNumber}/buildings/{buildingID}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addBuildingToManager(@PathVariable Long managerNumber, @PathVariable Long buildingID) throws NotFoundException, AlreadyExistsException {

        buildingService.addManager(buildingID, managerNumber);
        LOG.info("Manager with manager number {} added to building {}", managerNumber, buildingID);
    }

    @PatchMapping(value = "/{managerNumber}/buildings/{buildingID}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeBuildingFromManager(@PathVariable Long managerNumber, @PathVariable Long buildingID) throws NotFoundException{

        buildingService.removeManager(buildingID, managerNumber);
        LOG.info("Manager with manager number {} removed from block {}", managerNumber, buildingID);
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createManager(@RequestBody Manager manager) {

        managerService.create(manager);
        LOG.info("Manager with id {} and manager number {} created", manager.getId(), manager.getManagerNumber());
    }


    @PatchMapping(value = "/{managerNumber}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateManager(@PathVariable Integer managerNumber, @RequestBody Manager manager) throws NotFoundException, NotAllowedException {

        managerService.update(managerNumber, manager);
        LOG.info("Manager with manager number {} updated.", managerNumber);
    }

    @DeleteMapping(value = "/{managerNumber}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteManager(@PathVariable Integer managerNumber) throws NotFoundException {

        managerService.delete(managerNumber);
        LOG.info("Manager with manager number {} deleted.", managerNumber);
    }
}
