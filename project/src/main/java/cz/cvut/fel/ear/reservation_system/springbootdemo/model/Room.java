package cz.cvut.fel.ear.reservation_system.springbootdemo.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Room.find", query = "SELECT r FROM Room r WHERE r.building.id = :buildingID AND r.roomNumber = :roomNumber AND r.deleted_at is null")
})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Room extends AbstractEntity{

    @Basic(optional = false)
    @Column(nullable = false)
    @Min(value = 1, message = "Room number should not be less than 1")
    @Max(value = 999, message = "Room number should not be greater than 999")
    private Integer roomNumber;

    @Basic(optional = false)
    @Column(nullable = false)
    @PositiveOrZero(message = "Capacity of room can not be negative")
    private Integer maxCapacity;

    @ManyToOne
    @JoinColumn(name="building_id", nullable=true)
    private Building building;

    @OrderBy("dateStart DESC")
    @OneToMany
    private List<Reservation> reservations;

    public Building getBuilding() { return building; }

    public void setBuilding(Building building) { this.building = building; }

    public ArrayList<Reservation> getReservations() {
        if (reservations == null) reservations = new ArrayList<>();
        ArrayList<Reservation> reservationsOutput = new ArrayList<>(reservations);
        return reservationsOutput;
    }

    public void addReservations(Reservation reservation){
        if (reservations == null) reservations = new ArrayList<>();
        reservations.add(reservation);
    }

    public void cancelReservation(Reservation reservation){
        if (reservations.contains(reservation)){
            reservations.remove(reservation);
        }
    }

    public Integer getCapacity() { return maxCapacity; }

    public void setCapacity(Integer maxCapacity) { this.maxCapacity = maxCapacity; }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }
}

