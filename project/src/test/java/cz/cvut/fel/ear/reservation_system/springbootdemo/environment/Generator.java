package cz.cvut.fel.ear.reservation_system.springbootdemo.environment;

import cz.cvut.fel.ear.reservation_system.springbootdemo.model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import java.sql.Timestamp;

public class Generator {

    private static final Random RAND = new Random();

    private static Timestamp startDate = Timestamp.valueOf("2011-10-02 18:00:00");
    private static Timestamp endDate = Timestamp.valueOf("2011-10-02 18:30:00");


    public static int randomInt() {

        return RAND.nextInt();
    }


    public static boolean randomBoolean() {

        return RAND.nextBoolean();
    }

    public static Applicant generateApplicant() {

        final Applicant user = new Applicant();
        user.setFirstName("FirstName" + randomInt());
        user.setLastName("LastName" + randomInt());
        user.setUsername("username" + randomInt());
        user.setPassword(Integer.toString(randomInt()));
        user.setBirth(LocalDate.of(1990, 12, 12));
        user.setBankAccountNumber("AAAAAAAAAA2222222222");
        user.setEmail("username" + randomInt() + "@test.cz");
        return user;
    }

    public static Manager generateManager(){
        final Manager manager = new Manager();
        manager.setFirstName("FirstName" + randomInt());
        manager.setLastName("Lastname" + randomInt());
        manager.setEmail("manager" + randomInt() + "@test.cz");
        manager.setUsername("managername" + randomInt());
        manager.setPassword(Integer.toString(randomInt()));
        return manager;
    }

    public static Building generateBuilding() {

        Building building = new Building();
        building.setName("WeWork");
        building.setAddress("Národní 14");
        building.addManager(generateManager());
        return building;

    }

    public static Room generateRoom(){

        Room room = new Room();
        room.setRoomNumber(207);
        room.setCapacity(20);
        return room;

    }

    public static Reservation generateReservation(Room room,Applicant applicant, Timestamp start, Timestamp end) {

        Reservation res = new Reservation();
        res.setDateStart(start);
        res.setDateEnd(end);
        res.setRoom(room);
        res.setApplicant(applicant);
        return res;
    }


}
