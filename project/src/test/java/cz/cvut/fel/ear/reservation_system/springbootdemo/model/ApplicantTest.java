package cz.cvut.fel.ear.reservation_system.springbootdemo.model;

import cz.cvut.fel.ear.reservation_system.springbootdemo.environment.Generator;
import cz.cvut.fel.ear.reservation_system.springbootdemo.util.Status;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ApplicantTest {

    static Applicant applicant;
    static Building building;
    static Reservation reservation;
    static Room room;

    @BeforeClass
    public static void init(){
        building = Generator.generateBuilding();
    }

    @Before
    public void beforeInit(){
        applicant = Generator.generateApplicant();
    }

    @Test
    public void hasActiveReservation_has_workCorrect(){
        room = Generator.generateRoom();
        reservation = Generator.generateReservation(room, applicant, Timestamp.valueOf("2010-10-30 11:55"), Timestamp.valueOf("2010-10-30 18:55"));
        reservation.setStatus(Status.ACTIVE);
        applicant.addReservation(reservation);
        assertTrue(applicant.hasActiveReservation());
    }

    @Test
    public void hasActiveReservation_hasNot_workCorrect(){
        assertFalse(applicant.hasActiveReservation());
    }
}
